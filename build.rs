// Build script to generate the PROST protobuf files

use std::io::Result;
fn main() -> Result<()> {
    prost_build::compile_protos(&["src/mmi/mmi.proto"], &["src/mmi/"])?;
    Ok(())
}
