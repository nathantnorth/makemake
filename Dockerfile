# Build and test docker file

FROM rust:1.56-slim-bullseye

RUN apt-get update -y \
    && apt-get upgrade -y

COPY . .

RUN cargo test -- --nocapture \
    && cargo build
