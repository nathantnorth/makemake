// Revolver

use crate::arsenal::chamber;
use std::time::SystemTime;

#[derive(Default)]
pub struct Revolver {
    cylinder: Vec<chamber::Chamber>,
    priority: i32,
}

impl Revolver {
    /// new creates a new revolver data structure which acts like a queue with
    /// a fixed capacity
    pub fn new(priority: i32) -> Revolver {
        let mut r = Revolver::default();
        r.priority = priority;
        // implicit return of r
        r
    }

    pub fn get_priority(&self) -> i32 {
        self.priority
    }

    pub fn weight(&self) -> usize {
        if let Some(ch) = self.cylinder.last() {
            let dur = ch.duration(SystemTime::now());
            return dur * self.priority as usize;
        }
        // return 0 if full is empty
        0
    }

    // borrow pops the last chamber from the cylinder
    pub fn borrow(&mut self) -> Option<chamber::Chamber> {
        self.cylinder.pop()
    }

    /// replace inserts the chamber
    pub fn replace(&mut self, mut ch: chamber::Chamber) {
        ch.stamp_the_time();
        self.cylinder.push(ch);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_new_revolver() {
        let r = Revolver::new(10);
        assert_eq!(r.cylinder.len(), 0);
    }

    #[test]
    fn check_revolver_weight() {
        let mut r = Revolver::new(10);
        let w = r.weight();
        assert_eq!(w, 0);

        let mut ch = chamber::Chamber::new(String::from("abc123"), 1000);
        ch.push(Vec::from([1, 2, 3, 4])).unwrap();
        r.replace(ch);

        let ten_millis = std::time::Duration::from_millis(10);
        std::thread::sleep(ten_millis);

        let w = r.weight();
        println!("Weight is: {}", w);
        assert_ne!(w, 0);

    }
}
