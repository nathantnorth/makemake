// Arsenal implementation

pub mod armory;
pub mod revolver;
pub mod chamber;
use crate::mmerr;

pub trait Arsenal {
    fn borrow(&mut self, full: bool) -> Option<chamber::Chamber>;
    fn replace(&mut self, ch: chamber::Chamber) -> Result<(), mmerr::FullError>;
}

pub trait Round {
    fn load(&mut self, data: Vec<u8>) -> Result<(), mmerr::FullError>;
}
