// Chamber data structure

use crate::mmi;
use crate::mmerr;
use std::time::SystemTime;

pub struct Chamber {
    p: mmi::Pakg,
    build_time: SystemTime,
}

impl Chamber {
    /// new creates a new chamber and initializes the underlying data vector to the
    /// length given.
    pub fn new(
        device_id: String,
        chamber_size: usize
    ) -> Chamber {
        let mut p = mmi::Pakg::default();
        let mut ph = mmi::PHead::default();
        ph.device_id = device_id;
        p.header = Some(ph);
        // allocate memory and set it to 0
        p.data = Vec::with_capacity(chamber_size);

        // implicit return of the chamber
        Chamber{
            p: p,
            build_time: SystemTime::now(),
        }
    }

    pub fn len(&self) -> usize {
        self.p.local_length as usize
    }

    pub fn set_priority(&mut self, priority: i32) {
        match &mut self.p.header {
            None => {
                let mut ph = mmi::PHead::default();
                ph.priority = priority;
                self.p.header = Some(ph);
            },
            Some(ph) => {
                ph.priority = priority;
            }
        }
    }

    pub fn get_priority(&self) -> Option<i32> {
        match &self.p.header {
            None => None,
            Some(h) => {
                Some(h.priority)
            }
        }
    }

    /// duration returns the number of milliseconds since the chamber was
    /// last updated
    pub fn duration(&self, st: SystemTime) -> usize {
        match st.duration_since(self.build_time) {
            Ok(n) => {
                let m = n.as_millis() as usize;
                if m == 0 {
                    // round up to 1 if a full millisecond hasn't passed yet
                    return 1;
                }
                m
            },
            Err(_) => 0,
        }
    }

    /// stamp_the_time is used to update the build time of the chamber
    pub fn stamp_the_time(&mut self) {
        self.build_time = SystemTime::now();
    }

    pub fn push(&mut self, data: Vec<u8>) -> Result<(), mmerr::FullError> {
        let dlen = data.len();
        for elem in data {
            self.p.data.push(elem);
        }
        self.p.local_length = dlen as i32;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_new_chamber() {
        let ch = Chamber::new(String::from("abc123"), 1000);
        assert_eq!(ch.p.local_length, 0);
    }
}
