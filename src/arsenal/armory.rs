// Armory implementation

use crate::arsenal::revolver;
use crate::arsenal::chamber;
use crate::mmerr;

#[derive(Default)]
pub struct Armory {
    supply: Vec<chamber::Chamber>,
    rooms: Vec<revolver::Revolver>,
    count: usize,
    capacity: usize,
}

impl Armory {
    pub fn new<'a>(
        device_id: &'a str,
        priorities: &'a mut [i32],
        chamber_size: usize,
        chamber_count: usize,
    ) -> Result<Armory, mmerr::ValueError> {
        let mut a = Armory::default();
        a.count = chamber_count;
        a.capacity = chamber_count;
        // sort the priorities so the revolvers are added in the correct order!
        priorities.sort();
        // let plen = priorities.len();
        for pri in priorities {
            if *pri <= 0 {
                // priority must be greater than 0
                return Err(mmerr::ValueError::new("priorities must be greater than 0"));
            }
            let r = revolver::Revolver::new(*pri);
            a.rooms.push(r);

            for _ in 0..chamber_size {
                a.supply.push(chamber::Chamber::new(String::from(device_id), chamber_size));
            }
        }
        // return a here
        Ok(a)
    }

    /// borrow is used to pop a chamber from the respective queue. if "full" is
    /// set to true, the next chamber (based on priority) with data in it
    /// is returned. Otherwise, if full is set to false, if there is a
    /// free empty chamber, that chamber is returned. full or empty chambers
    /// can be returned using the "replace" method
    pub fn borrow(&mut self, full: bool) -> Option<chamber::Chamber> {
        if !full {
            // get an empty chamber from the supply
            match self.supply.pop() {
                None => return None,
                Some(ch) => {
                    self.count -= 1;
                    return Some(ch);
                },
            }
        }
        // get the next full chamber with the highest weight
        let mut w = 0;
        let mut idx = 0;
        for (i, r) in self.rooms.iter().enumerate() {
            let tmpw = r.weight();
            if tmpw > w {
                w = tmpw;
                idx = i;
            }
        }
        if w == 0 {
            // all revolvers have 0 weight
            return None;
        }
        if let Some(rmax) = self.rooms.get_mut(idx) {
            match rmax.borrow() {
                None => return None,
                Some(ch) => {
                    self.count -= 1;
                    return Some(ch);
                }
            }
        }
        None
    }

    /// replace is used by all threads to give a pakg back to the armory
    pub fn replace(&mut self, ch: chamber::Chamber) -> Result<(), mmerr::FullError> {
        if self.count >= self.capacity {
            // already have too many chambers... need to "borrow" first
            return Err(mmerr::FullError::new("too many chambers... need to borrow first"));
        }
        if ch.len() == 0 {
            // this chamber is empty
            self.supply.push(ch);
            self.count += 1;
            return Ok(());
        }

        // full chamber... push to appropriate revolver
        match ch.get_priority() {
            None => Err(mmerr::FullError::new("priority must be defined if data present")),
            Some(p) => {
                let idx = self.find_closest_revolver_idx(p, 0, self.rooms.len()-1);
                self.rooms[idx].replace(ch);
                self.count += 1;
                Ok(())
            }
        }
    }

    fn find_closest_revolver_idx(&self, priority: i32, start: usize, end: usize) -> usize {
        if start == end {
            // the endpoints are the same
            return start;
        }
        let middle = ((end - start) / 2) + start + 1; // avoids overflow
        let middlepri = self.rooms[middle].get_priority();
        if middlepri == priority {
            return middle;
        } else if middlepri < priority {
            return self.find_closest_revolver_idx(priority, middle, end);
        }
        self.find_closest_revolver_idx(priority, start, middle-1)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_new_armory() {
        let mut priorites = vec![1, 5, 9, 2, 3];
        let mut a = Armory::new("abc123", &mut priorites, 1000, 10).unwrap();
        assert_eq!(priorites, vec![1, 2, 3, 5, 9]);

        if let Some(_) = a.borrow(true) {
            panic!("there should not be any full chambers yet!");
        }

        if let Some(mut ch) = a.borrow(false) {
            // get an empty chamber
            ch.push(vec![1, 99, 34, 24, 29, 19]).unwrap();
            ch.set_priority(5);
            a.replace(ch).unwrap();
        }
    }
}
