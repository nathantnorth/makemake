// Makemake binary application

use log::{debug, error, log_enabled, info, Level};

// mmi is the makemake interface module
pub mod mmi {
    include!(concat!(env!("OUT_DIR"), "/makemake.mmi.rs"));
}
mod mmerr;
mod arsenal;

#[tokio::main]
async fn main() {
    env_logger::init();
    info!("Makemake starting...");
}
