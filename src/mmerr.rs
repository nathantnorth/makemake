// Errors within Makemake

use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct FullError {
    details: String
}

impl FullError {
    pub fn new(msg: &str) -> FullError {
        FullError{details: msg.to_string()}
    }
}

impl fmt::Display for FullError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"{}",self.details)
    }
}

impl Error for FullError {
    fn description(&self) -> &str {
        &self.details
    }
}


// ValueError
#[derive(Debug)]
pub struct ValueError {
    details: String
}

impl ValueError {
    pub fn new(msg: &str) -> ValueError {
        ValueError{details: msg.to_string()}
    }
}

impl fmt::Display for ValueError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"{}",self.details)
    }
}

impl Error for ValueError {
    fn description(&self) -> &str {
        &self.details
    }
}
