# Makemake

Named after the Kuiper belt object! Referred to as **MM** here on.

## Design
**MM** is a multi-threaded agent and set of libraries which are intended to run on an edge device and communicate offboard for IOT applications. It allocates all of its memory up front and performs some system checks to validate it's operation.

1. Client streams data over TCP to **MM** using a persistent connection over the local network.
2. As the data comes in, **MM** breaks it apart into tangible chunks (if not already in pleasantly sized chunks)
3. **MM** attempts to send the messages offboard at the same time. It also simultaneously tries to write the incoming data to a file for persistance.
4. The chunks


Individual messages are stored in a `Chamber`. All the chambers are within the `Revolver`. The `Armory` contains sets of `Revolver`s each with different priorities. A `Transporter` thread accepts the

## Modes
- Store and Forward:
  - Don't Store
  - Store if space exists
  - Overwrite lower priority messages
  - Overwrite any/oldest message
